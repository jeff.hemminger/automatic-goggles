This compose file is configured to run TitanDB in with Cassandra and ElasticSearch in TitanDB's Remote Server mode.

## to build

```
$ docker-compose build
```

## to run

Individually, run:

```
$ docker-compose up -d cassandra
```

And elasticsearch:

```
$ docker-compose up -d es
```

And titan:

```
$ docker-compose up -d titan
```

Or, for the sake of brevity,

```
$ docker-compose up -d
```

To connect with gremlin:

```
$ docker-compose run titan bash

// connects to titan container
$$ ./bin/gremlin.sh
...
plugin activated: tinkerpop.tinkergraph
gremlin> graph = TitanFactory.build().set("storage.backend", "cassandrathrift").set("storage.hostname", "cassandra").open();
==>standardtitangraph[cassandrathrift:[cassandra]]
gremlin> graph.addVertex('human')
==>v[4160]
```

## Bag of Tricks

### ElasticSearch

- Port 9200 is exposed: ```curl http://localhost:9200/_count?pretty```
- The Marvel plugin is installed, location is http://localhost:9200/_plugin/marvel/sense/index.html

### Cassandra

There are no ports currently open outside the container and service network, so attach to the container
and run cqlsh:

```
$ docker-compose exec cassandra cqlsh
```
