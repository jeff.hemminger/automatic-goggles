/* goggles-schema.groovy
 *
 * Helper functions for declaring Titan schema elements
 * (vertex labels, edge labels, property keys) to accommodate TP3 data.
 *
 * Sample usage in a gremlin.sh session:
 *
 * gremlin> :load scripts/goggles-schema.groovy
 * ==>true
 * ==>true
 * gremlin> t = TitanFactory.open('conf/titan-cassandra.properties')
 * ==>standardtitangraph[cassandrathrift:[127.0.0.1]]
 * gremlin> defineGogglesSchema(t)
 * ==>null
 * gremlin> t.close()
 * ==>null
 * gremlin>
 */

def defineGogglesSchema(titanGraph) {
    m = titanGraph.openManagement()
    // vertex labels
    person = m.makeVertexLabel("person").make()
    vehicle   = m.makeVertexLabel("vehicle").make()
    town = m.makeVertexLabel("town").make()

    // edge labels
    friendOf    = m.makeEdgeLabel("friendOf").make()
    vehicleOf  = m.makeEdgeLabel("vehicleOf").make()
    livesIn = m.makeEdgeLabel("livesIn").make()

    // vertex and edge properties
    blid = m.makePropertyKey("bulkLoader.vertex.id").dataType(Long.class).make()
    name = m.makePropertyKey("name").dataType(String.class).make()
    firstName = m.makePropertyKey("firstName").dataType(String.class).make()
    lastName = m.makePropertyKey("lastName").dataType(String.class).make()
    japaneseName     = m.makePropertyKey("japaneseName").dataType(String.class).make()
    prefecture = m.makePropertyKey("prefecture").dataType(String.class).make()
    population = m.makePropertyKey("population").dataType(Integer.class).make()
    area = m.makePropertyKey("area").dataType(Long.class).make()
    density = m.makePropertyKey("density").dataType(Integer.class).make()
    founded = m.makePropertyKey("founded").dataType(Date.class).make()
    age = m.makePropertyKey("age").dataType(Integer.class).make()

    // global indices
    m.buildIndex("byBulkLoaderVertexId", Vertex.class).addKey(blid).buildCompositeIndex()
    m.buildIndex("peopleByName", Vertex.class).addKey(name).indexOnly(person).buildCompositeIndex()
    m.buildIndex("byFirstName", Vertex.class).addKey(firstName).indexOnly(person).buildCompositeIndex()
    m.buildIndex("byLastName", Vertex.class).addKey(lastName).indexOnly(person).buildCompositeIndex()

    // vertex centric indices
    m.buildEdgeIndex(friendOf, "friendOfName", Direction.BOTH, Order.decr, name)
    m.commit()
}
